dmg=$1
ipath=$2

function usage {
	echo "USAGE: $0 file.dmg [installation path]"
}

if [[ $# -eq 0 || $# -gt 2 ]]; then
	echo "ERROR: invalid number of arguments"
	usage
	exit -1	
fi

if [[ -d $ipath ]]; then
	echo "ERROR: $ipath directory already present"
	usage
	exit -1	
fi

if [[ ! -f $dmg ]]; then
	echo "ERROR: $dmg not a file"
	usage
	exit -1
fi

# A script to install Unity3d automatically from the command line given a dmg file.
# The resulting file is stored under /Applications/Unity$VERSION

# check assumptions
unityhome=/Applications/Unity

if [[ -d "$unityhome" ]]; then
    echo "ERROR: $unityhome already present"
    exit -1
fi

tempfoo=`basename $0`
TMPFILE=`mktemp /tmp/${tempfoo}.XXXXXX` || exit 1
hdiutil verify $dmg || exit 1
hdiutil mount -readonly -nobrowse -plist $dmg > $TMPFILE
vol=`grep Volumes $TMPFILE  | sed -e 's/.*>\(.*\)<\/.*/\1/'`
pkg=`ls -1 "$vol"/*.pkg`
installer -verbose -pkg "$pkg" -dominfo -volinfo -pkginfo
sudo installer -pkg "$pkg" -target /
hdiutil unmount "$vol"

if [[ ! -d "$unityhome" ]]; then
    echo "ERROR: $unityhome not present after installation. Something went wrong"
    exit -1
fi

unityversion=`grep -A 1 CFBundleVersion "$unityhome"/Unity.app/Contents/Info.plist | grep string | sed -e 's/.*>\(.*\)<\/.*/\1/'`
if [[ -d "$unityhome$unityversion" ]]; then
    echo "ERROR: "$unityhome$unityversion" already present on disk. Something went wrong"
    sudo rm -rf "$unityhome"
    exit -1
fi

if [[ -z "$ipath" ]]; then
	ipath="$unityhome$unityversion"
fi

sudo mv "$unityhome" "$ipath"

echo "Unity $unityversion installed at $ipath"
