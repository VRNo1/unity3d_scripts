A collection of scripts to install and run Unity3d from the command line on Mac or Windows.

Usage
------

* install_unity3d.sh

To give admin privileges, run sudo visudo and add a line like (path depends on your local installation)

YourUser ALL=(ALL) NOPASSWD: /Users/YourUser/bin/install_unity3d.sh

Then to install in standard directory

sudo /Users/YourUser/bin/install_unity3d.sh unity-4.2.1.dmg

* install_unity3d.bat

** install in standard directory

install_unity3d.bat UnitySetup-4.5.0.exe

** install in custom directory

install_unity3d.bat UnitySetup-4.5.0.exe C:\Apps\Unity4.5.0
